**TrafficLights - an Arduino traffic light controller.**

This is the source code for an arduino traffic light controller - see [my blog post](http://samritchie.net/2013/10/16/build-server-traffic-lights/) for the hardware details, but in short, it was designed for an Arduino Ethernet with the Seeed Studios Relay Shield V2.0. It was originally based on Dirk Engel’s [ArduinoTrafficLights source](https://github.com/DirkEngels/ArduinoTrafficLights).

It requires a fixed Arduino 1.0 version of [Georg Kaindl’s EthernetBonjour library](http://gkaindl.com/software/arduino-ethernet/bonjour) (I used [Michael Vogt's version](https://github.com/neophob/EthernetBonjour)), with the `HAS_SERVICE_REGISTRATION` and `HAS_NAME_BROWSING` preprocessor constants both disabled.

This code is open sourced under the terms of the [ISC license](http://opensource.org/licenses/ISC).