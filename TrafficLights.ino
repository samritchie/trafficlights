/*
 Traffic Lights
 
 A web server that controls a set of (real) traffic lights.

 Based on Dirk Engels' traffic light controller.
 
 Copyright (c) 2013, Sam Ritchie sam@samritchie.net

 Permission to use, copy, modify, and/or distribute this software for any purpose with or 
 without fee is hereby granted, provided that the above copyright notice and this permission 
 notice appear in all copies.

 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO 
 THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT 
 SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY 
 DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF 
 CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE 
 OR PERFORMANCE OF THIS SOFTWARE.
 
 */

#include <SPI.h>
#include <Ethernet.h>
#include <EthernetBonjour.h>

#define STRING_BUFFER_SIZE 128
#define RED_PIN 4
#define AMBER_PIN 5
#define GREEN_PIN 6
#define FLASH_TIMEOUT 1000UL
#define BUILD_SERVER_TIMEOUT 300000UL

int current = 4;
unsigned long lastUpdate = 0;
unsigned long lastFlash = 0;
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
EthernetServer server(80);
boolean flashing = false;

PROGMEM prog_char html_header[] = "<!DOCTYPE html>\n<html>\n<head>\n  <title>Traffic Light</title>\n  <style type=\"text/css\">\n    body {\n      background-color: #ddffff;\n      font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;\n      text-align: center;\n    }\n    #lights {\n      background-color: #555555;\n      margin-left: auto;\n      margin-right: auto;\n      width: 100px;\n      border-radius: 10px; \n      border: 10px solid #333333;\n      padding:20px;\n    }\n    .green {\n      background-image: -webkit-radial-gradient(45px 45px, circle cover, #00ff00, #008000);\n    }\n    .green.off {\n      background-image: -webkit-radial-gradient(45px 45px, circle cover, #006600, #003300);\n    }\n    .red {\n      background-image: -webkit-radial-gradient(45px 45px, circle cover, #ff0000, #800000);\n    }\n    .red.off {\n      background-image: -webkit-radial-gradient(45px 45px, circle cover, #800000, #400000);\n    }\n    .amber {\n      background-image: -webkit-radial-gradient(45px 45px, circle cover, #ffff99, #eebb00);\n    }\n    .amber.off {\n      background-image: -webkit-radial-gradient(45px 45px, circle cover, #808000, #4c4c00);\n    }\n    .circle {\n      margin-left: auto;\n      margin-right:auto;\n      margin-top: 5px;\n      margin-bottom: 5px;\n      border-radius: 50%;\n      width: 75px;\n      height: 75px;\n    }\n  </style>\n</head>\n<body>\n  <h1>Traffic Light</h1>\n  ";
PROGMEM prog_char flash_script[] = "<script>\n    setInterval(function() {\n      var e = document.getElementById(\"amber\");\n      if (e.className.indexOf(\"off\") != -1) {\n        e.className = e.className.replace(\" off\", \"\");\n      } else {\n        e.className += \" off\";\n      }\n    }, 1000);\n  </script>";
PROGMEM prog_char html_footer[] = "\n</body>\n</html>";

#define HTML_HEADER 0
#define HTML_FLASHING_SCRIPT 1
#define HTML_FOOTER 2
PGM_P html_blocks[] PROGMEM = { html_header, flash_script, html_footer };

void setup() {
  // Initialize output pins
  pinMode(RED_PIN, OUTPUT);
  pinMode(AMBER_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
  
  // Setup serial debugging support
  Serial.begin(9600);
  Serial.println("Initialising device");

  // Initalize the Ethernet connection
  Serial.print("Initialising ethernet connection... ");
  if (Ethernet.begin(mac)) {
    Serial.println(Ethernet.localIP());

    Serial.print("Initialising Bonjour... ");
    if (EthernetBonjour.begin("traffic-light")) {
      EthernetBonjour.run();
      Serial.println("traffic-light.local");
    } else {
      Serial.println("ERROR");
      Serial.println("Bonjour could not be initialised");
    }
    // Start the webserver
    Serial.println("Initialising webserver");
    server.begin();
    
    // Loop leds 3x3 times
    Serial.println("Testing all the lights");
    for(int i = RED_PIN; i <= GREEN_PIN; i++) {
      switchTo(i);
      delay(1000);
    }

  } else {
    Serial.println("ERROR");
    Serial.println("Could not obtain an IP address");
  }
  
    
  Serial.println("Setup complete");
  Serial.println("");
  startFlashing();
}

void loop() {
  String requestFirstLine = "";
  // listen for incoming clients
  EthernetClient client = server.available();

  if (client) {
    // Print debug
    Serial.println("Accepted client");

    // An http request ends with a blank line
    boolean currentLineIsBlank = true;
    // Only the first line of the request is needed
    boolean firstLine = true;

    while (client.connected()) {
      if (client.available()) {
        // Append the character to get the uri string
        char c = client.read();
        if ((firstLine) && (c != '\n')) {
          requestFirstLine += c;
        }

        // If you've gotten to the end of the line (received a newline
        // character) and the line is blank, the http request has ended,
        // so the reply can be sent
        if (c == '\n' && currentLineIsBlank) {
          Serial.println(requestFirstLine);
          String request = String(requestFirstLine);
          // TODO: flashing?
          if (request.startsWith("POST /amber")) {
            stopFlashing();
            switchTo(AMBER_PIN);
            lastUpdate = millis();
            redirect(client);
          } else if (request.startsWith("POST /red")) {
            stopFlashing();
            switchTo(RED_PIN);
            lastUpdate = millis();
            redirect(client);
          } else if (request.startsWith("POST /green")) {
            stopFlashing();
            switchTo(GREEN_PIN);
            lastUpdate = millis();
            redirect(client);
          } else if (request.startsWith("GET / ")) {
            index(client);
          } else {
            notFound(client);
          }
          break;
        }
        if (c == '\n') {
          // New line found
          currentLineIsBlank = true;
          firstLine = false;
        } 
        else if (c != '\r') {
          // Next line contains a character
          currentLineIsBlank = false;
        }
      }
    }

    // Give the web browser time to receive the data
    delay(10);

    // close the connection:
    client.stop();
    Serial.println("");
  }
  
  // Bonjour
  EthernetBonjour.run();
  
  // Start flashing if we haven't heard from the build server
  if ((millis() - lastUpdate) >= BUILD_SERVER_TIMEOUT) {
    startFlashing();
  }
  flash();
}

/**
 * switchTo
 * - Switch off previous led
 * - Switch on next led
 */
void switchTo(unsigned int light) {
  int prev = current;
  current = light;

  Serial.print("- Switching leds ");
  Serial.print(prev);
  Serial.print(" => ");
  Serial.println(current);

  digitalWrite(prev, LOW);
  digitalWrite(current, HIGH); 
}

/**
 * startFlashing
 * - set up pins & global state for flashing
 */
void startFlashing() {
  if (!flashing) {
    Serial.println("Start flashing amber.");
    digitalWrite(current, LOW);
    current = 0;
    flashing = true;
    flash();
  }
}

/**
 * stopFlashing
 * - revert pins & global state from flashing
 */
void stopFlashing() {
  if (flashing) {
    Serial.println("Stop flashing amber.");
    flashing = false;
    digitalWrite(AMBER_PIN, LOW);
  }
}

/**
 * flash
 * - check to see if the amber light needs to be toggled. 
 * Should be called often (ie once per loop).
 */
void flash() {
  if (flashing && (millis() - lastFlash) >= FLASH_TIMEOUT) {
    if (digitalRead(AMBER_PIN) == HIGH)
      digitalWrite(AMBER_PIN, LOW);
    else
      digitalWrite(AMBER_PIN, HIGH);
     lastFlash = millis();
  }
}

/**
 * Output
 * - content split by buffer size
 */
void output(EthernetClient client, char *realword) {
  int total = 0;
  int start = 0;
  char buffer[STRING_BUFFER_SIZE];
  int realLen = strlen_P(realword);

  memset(buffer,0,STRING_BUFFER_SIZE);

  while (total <= realLen) {
    // print content
    strncpy_P(buffer, realword+start, STRING_BUFFER_SIZE-1);
    client.print(buffer);

    // more content to print?
    total = start + STRING_BUFFER_SIZE-1;
    start = start + STRING_BUFFER_SIZE-1;
  }
}

/**
 * Index
 * - writes HTML index page, based on the current state of the pins
 */
void index(EthernetClient client) {
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println();  
  output(client,(char*)pgm_read_word(&(html_blocks[HTML_HEADER])));
  
  client.println("<div id=\"lights\">");
  renderLight(client, "red", (current == RED_PIN));
  renderLight(client, "amber", (current == AMBER_PIN));
  renderLight(client, "green", (current == GREEN_PIN));
  client.println("</div>");
  if (flashing)
    output(client,(char*)pgm_read_word(&(html_blocks[HTML_FLASHING_SCRIPT])));
  output(client,(char*)pgm_read_word(&(html_blocks[HTML_FOOTER])));
}

/**
 * renderLight
 * - writes the light div and form out to the response stream
 */
void renderLight(EthernetClient client, char *colour, boolean active) {
  String lightTemplate = String("<div class=\"circle {{colour}} {{state}}\" id=\"{{colour}}\"></div>\n<form method=\"POST\" action=\"/{{colour}}\"><input type=\"submit\" value=\"Switch\"/></form>\n");
  lightTemplate.replace("{{colour}}", colour);
  if (active)
    lightTemplate.replace("{{state}}", "");
  else
    lightTemplate.replace("{{state}}", "off");
  client.println(lightTemplate);
}

/**
 * NotFound
 * - writes HTML 404 header
 */
void notFound(EthernetClient client) {
  client.println("HTTP/1.1 404 Not Found");
  client.println("Content-Type: text/html");
  client.println();
}

/**
 * Redirect
 * - writes HTML redirect to index page
 */
void redirect(EthernetClient client) {
  client.println("HTTP/1.1 302 Found");
  client.println("Location: /");
  client.println();  
}
